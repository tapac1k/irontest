package com.example.myapplication.di

import com.example.domain.interactors.contacts.GetContactsUseCase
import com.example.myapplication.feature.contacts.ContactViewModel
import com.example.myapplication.feature.contacts.ContactsAdapter
import com.example.myapplication.feature.main.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val contactsModule = module {

    factory {
        GetContactsUseCase(get(), AndroidSchedulers.mainThread(), Schedulers.io())
    }


    factory {
        ContactsAdapter()
    }

    viewModel {
        ContactViewModel(get())
    }


}