package com.example.myapplication.di

import com.example.data.sources.api.ActionResponseMapper
import com.example.domain.ActionMapper
import com.example.domain.interactors.action.GetActionUseCase
import com.example.myapplication.feature.main.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    factory { ActionMapper() }
    factory { ActionResponseMapper() }

    factory { GetActionUseCase(get(), get(), get(), get(), AndroidSchedulers.mainThread(), Schedulers.io()) }
    viewModel {
        MainViewModel(get())
    }
}