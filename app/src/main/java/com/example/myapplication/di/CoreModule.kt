package com.example.myapplication.di

import com.example.data.ApiRepositoryImpl
import com.example.data.LocalRepositoryImpl
import com.example.data.PhoneRepositoryImpl
import com.example.data.sources.api.ApiService
import com.example.data.sources.contacts.ContactsProvider
import com.example.domain.repository.ApiRepository
import com.example.domain.repository.LocalRepository
import com.example.domain.repository.PhoneRepository
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val coreModule = module {
    single { ContactsProvider(get()) }

    single { PhoneRepositoryImpl(get(), get()) as PhoneRepository }

    single { LocalRepositoryImpl() as LocalRepository}

    single { ApiRepositoryImpl(get(),get()) as ApiRepository}

    single {
        Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://s3-us-west-2.amazonaws.com/")
            .build()
            .create(ApiService::class.java)
    }
}