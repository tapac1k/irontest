package com.example.myapplication.feature.main

import androidx.lifecycle.MutableLiveData
import com.example.domain.entities.*
import com.example.domain.interactors.action.GetActionUseCase
import com.example.myapplication.common.CommonViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

class MainViewModel(
    private val getActionUseCase: GetActionUseCase
) : CommonViewModel<MainFragment.Event>() {

    val compositeDisposable = CompositeDisposable()
    val actionLoading = MutableLiveData(false)

    fun loadAction() {
        actionLoading.postValue(true)
        compositeDisposable += getActionUseCase.execute(onSuccess = {
            runAction(it)
            actionLoading.postValue(false)
        },
            onError = {
                actionLoading.postValue(false)
            })
    }

    fun runAction(action: Action) {
        when (action) {
            is AnimateAction -> notifyEvent(MainFragment.Event.ANIMATE)
            is ToastAction -> notifyEvent(MainFragment.Event.SHOW_TOAST)
            is CallAction ->  notifyEvent(MainFragment.Event.NAVIGATE_CONTACTS)
            is NotificationAction -> notifyEvent(MainFragment.Event.SHOW_NOTIFICATION)
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}