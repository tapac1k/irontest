package com.example.myapplication.feature.contacts

import androidx.databinding.ObservableField
import com.example.domain.entities.Contact

class ItemContactViewModel(private val onClickListener: ((Contact) -> Unit)?) {

    val contact = ObservableField<Contact>()

    fun bind(contact: Contact) {
        this.contact.set(contact)
    }

    fun onClick() {
        onClickListener?.invoke(contact.get()!!)
    }
}