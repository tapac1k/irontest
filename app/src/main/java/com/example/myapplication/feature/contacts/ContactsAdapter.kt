package com.example.myapplication.feature.contacts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.entities.Contact
import com.example.myapplication.databinding.ContactItemContactBinding


class ContactsAdapter() : ListAdapter<Contact, ContactViewHolder>(ContactDiffCallback()) {
    var onContactClickListener: ((Contact) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ContactItemContactBinding.inflate(inflater, parent, false)
        return ContactViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact = getItem(position)

        if (holder.binding.contact == null) {
            holder.binding.contact =
                ItemContactViewModel(
                    onContactClickListener
                )
        }

        holder.binding.contact!!.bind(contact)
    }
}

class ContactDiffCallback : DiffUtil.ItemCallback<Contact>() {
    override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
        return oldItem == newItem

    }
}

class ContactViewHolder(val binding: ContactItemContactBinding) : RecyclerView.ViewHolder(binding.root)