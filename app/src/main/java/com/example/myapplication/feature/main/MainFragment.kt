package com.example.myapplication.feature.main

import android.app.PendingIntent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.NavDeepLinkBuilder
import androidx.navigation.Navigation
import com.example.myapplication.R
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.example.myapplication.databinding.MainFragmentBinding

class MainFragment: Fragment() {
    lateinit var binding: MainFragmentBinding
    private lateinit var navController: NavController
    private val viewModel by viewModel<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel
        viewModel.events.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotConsumed()?.run { onEvent(this) }
        } )
        navController = Navigation.findNavController(binding.root)

    }

    fun onEvent(event: Event) {
        when(event) {
            Event.NAVIGATE_CONTACTS -> {
                val action = MainFragmentDirections.actionMainFragmentToContactFragment()
                navController.navigate(action)
            }
            Event.ANIMATE -> binding.btnAction.animate().rotationBy(360f).start()
            Event.SHOW_NOTIFICATION -> showNotification()
            Event.SHOW_TOAST -> Toast.makeText(requireContext(), R.string.toast, Toast.LENGTH_SHORT).show()
        }
    }

    private fun showNotification() {
        with(requireContext()) {
            val pendingIntent = NavDeepLinkBuilder(this).setGraph(R.navigation.main_nav)
                .setDestination(R.id.contactFragment)
                .createPendingIntent()
            val builder = NotificationCompat.Builder(this, "main_channel")
                .setSmallIcon(com.example.myapplication.R.drawable.ic_launcher_foreground)
                .setContentTitle("Action is Notification!")
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            with(NotificationManagerCompat.from(requireContext())) {
                notify(0, builder.build())
            }

        }
    }

    enum class Event {
        NAVIGATE_CONTACTS,
        ANIMATE,
        SHOW_NOTIFICATION,
        SHOW_TOAST
    }
}