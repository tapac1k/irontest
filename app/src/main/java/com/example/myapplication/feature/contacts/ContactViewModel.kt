package com.example.myapplication.feature.contacts

import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import com.example.domain.entities.Contact
import com.example.domain.interactors.contacts.GetContactsUseCase
import com.example.myapplication.common.CommonViewModel

class ContactViewModel(
    private val getContactsUseCase: GetContactsUseCase
): CommonViewModel<ContactFragment.Event>() {

    val contactList = MutableLiveData<List<Contact>>()


    fun loadContacts() {
        getContactsUseCase.execute(onSuccess = {
            contactList.postValue(it)
        })
    }

    fun onContactClick(contact: Contact) {
        notifyEvent(ContactFragment.Event.CALL_USER, bundleOf("phone" to contact.phone))
    }
}