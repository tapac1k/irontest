package com.example.myapplication.feature.contacts

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.myapplication.databinding.ContactFragmentBinding
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.content.Intent
import android.net.Uri


private const val PERMISSIONS_REQUEST_READ_CONTACTS = 101
private const val PERMISSIONS_REQUEST_CALL = 102

class ContactFragment : Fragment() {
    lateinit var binding: ContactFragmentBinding

    private val viewModel by viewModel<ContactViewModel>()

    private val contactsAdapter: ContactsAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ContactFragmentBinding.inflate(inflater, container, false)
        with(binding.recyclerContacts) {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                context!!,
                androidx.recyclerview.widget.RecyclerView.VERTICAL,
                false
            )
            adapter = contactsAdapter
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel
        viewModel.events.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotConsumed()?.run { onEvent(this, it.extra) }
        })

        viewModel.contactList.observe(viewLifecycleOwner, Observer {
            contactsAdapter.submitList(it)
        })
        contactsAdapter.onContactClickListener = {
            viewModel.onContactClick(it)
        }
        loadData()
    }

    private fun loadData() {
        if (hasContactPermission()) {
            viewModel.loadContacts()
        } else {
            requestContactsPermissions()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_READ_CONTACTS -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    viewModel.loadContacts()
                } else {
                    //todo
                }
                return
            }
        }

    }

    fun startCall(phone: String) {
        if(hasCallPermission()) {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone))
            startActivity(intent)
        } else {
            requestCallPermissions()
        }
    }


    fun onEvent(event: Event, extras: Bundle?) {
        when (event) {
            Event.CALL_USER -> extras?.getString("phone")?.run { startCall(this) }
        }
    }

    private fun requestContactsPermissions() {
        requestPermissions(
            arrayOf(Manifest.permission.READ_CONTACTS),
            PERMISSIONS_REQUEST_READ_CONTACTS
        )
    }


    private fun hasContactPermission(): Boolean {
        return ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED
    }


    private fun requestCallPermissions() {
        requestPermissions(
            arrayOf(Manifest.permission.CALL_PHONE),
            PERMISSIONS_REQUEST_CALL
        )
    }


    private fun hasCallPermission(): Boolean {
        return ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CALL_PHONE
        ) == PackageManager.PERMISSION_GRANTED
    }


    enum class Event {
        CALL_USER
    }
}