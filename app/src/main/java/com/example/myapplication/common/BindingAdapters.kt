package com.example.myapplication.common

import android.graphics.drawable.ColorDrawable
import android.webkit.URLUtil
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

@BindingAdapter(value = ["android:avatarUrl", "android:placeholderText"], requireAll = false)
fun setImageUrlRound(imageView: ImageView, avatarUrl: String?, placeholderText: String?) {
    val url = avatarUrl ?: ""
    val resId = url.toIntOrNull()
    Glide
        .with(imageView.context)
        .load(
           avatarUrl
        )
        .apply(RequestOptions.circleCropTransform())
        .into(imageView)

}