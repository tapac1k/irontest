package com.example.myapplication.common

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.feature.main.MainFragment

open class CommonViewModel<Event : Enum<Event>>: ViewModel() {
    private val mutEvent = MutableLiveData<Consumable<Event>>()

    val events: LiveData<Consumable<Event>>
        get() = mutEvent

    protected fun notifyEvent(event: Event, extras: Bundle? = null) {
        mutEvent.postValue(Consumable(event, extras))
    }

}