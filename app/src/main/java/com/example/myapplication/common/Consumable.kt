package com.example.myapplication.common

import android.os.Bundle

class Consumable<out T>(private val content: T, val extra: Bundle?) {

    private var hasBeenConsumed = false


    fun getContentIfNotConsumed(): T? {
        return if (hasBeenConsumed) {
            null
        } else {
            hasBeenConsumed = true
            content
        }
    }
}