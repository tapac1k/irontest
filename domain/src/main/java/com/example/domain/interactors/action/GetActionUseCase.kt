package com.example.domain.interactors.action

import com.example.domain.ActionMapper
import com.example.domain.entities.Action
import com.example.domain.entities.ActionConfig
import com.example.domain.entities.None
import com.example.domain.interactors.NoParamSingleUseCase
import com.example.domain.repository.ApiRepository
import com.example.domain.repository.LocalRepository
import com.example.domain.repository.PhoneRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import java.util.*

class GetActionUseCase(
    private val apiRepository: ApiRepository,
    private val localRepository: LocalRepository,
    private val phoneRepository: PhoneRepository,
    private val actionMapper: ActionMapper,
    mainThread: Scheduler,
    ioThread: Scheduler
) : NoParamSingleUseCase<Action>(mainThread, ioThread) {

    override fun buildUseCaseObservable(): Single<Action> {
        return apiRepository.getActions().map {
            val action =
                it.filter {
                    isValid(it)
                }.maxBy {
                    it.priority
                } ?: return@map None()

            localRepository.setLastTimeChosen(action.type, System.currentTimeMillis()).blockingAwait()

            actionMapper.map(action.type)
        }
    }

    private fun isValid(action: ActionConfig): Boolean {
        if (!action.enabled) return false

        val calendar = Calendar.getInstance()
        val currentDay = calendar.get(Calendar.DAY_OF_WEEK) - 1
        if (!action.validDays.contains(currentDay)) return false

        val lastTimeActionUsed = localRepository.getLastTimeChosen(action.type).blockingGet()

        if (action.coolDown > System.currentTimeMillis() - lastTimeActionUsed) {
            return false
        }

        if (action.type == "toast" && !phoneRepository.internetConnectionEnabled().blockingGet()) {
            return false
        }

        return true
    }

}