package com.example.domain.interactors


import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver

abstract class NoParamSingleUseCase<Output>(
    private val mainThread: Scheduler,
    private val ioThread: Scheduler
) {

    internal abstract fun buildUseCaseObservable(): Single<Output>

    fun execute(
        onSuccess: ((Output) -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null
    ): DisposableSingleObserver<Output> {
        return this.buildUseCaseObservable()
            .subscribeOn(ioThread)
            .observeOn(mainThread)
            .subscribeWith(object : DisposableSingleObserver<Output>() {
                override fun onSuccess(t: Output) {
                    onSuccess?.invoke(t)
                }

                override fun onError(e: Throwable) {
                    onError?.invoke(e)
                }
            })
    }


}
