package com.example.domain.interactors.contacts

import com.example.domain.entities.Contact
import com.example.domain.interactors.NoParamSingleUseCase
import com.example.domain.repository.PhoneRepository
import io.reactivex.Scheduler
import io.reactivex.Single

class GetContactsUseCase(
    private val contactsRepository: PhoneRepository,
    private val mainThread: Scheduler,
    private val ioThread: Scheduler
): NoParamSingleUseCase<List<Contact>>(mainThread, ioThread) {

    override fun buildUseCaseObservable(): Single<List<Contact>> {
        return contactsRepository.getContacts()
    }
}