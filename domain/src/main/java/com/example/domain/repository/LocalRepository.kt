package com.example.domain.repository

import com.example.domain.entities.Action
import io.reactivex.Completable
import io.reactivex.Single

interface LocalRepository {
    fun getLastTimeChosen(action: String): Single<Long>
    fun setLastTimeChosen(action: String, time: Long): Completable
}