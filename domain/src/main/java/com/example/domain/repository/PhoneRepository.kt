package com.example.domain.repository

import com.example.domain.entities.Contact
import io.reactivex.Single

interface PhoneRepository {
    fun getContacts(): Single<List<Contact>>
    fun internetConnectionEnabled(): Single<Boolean>
}