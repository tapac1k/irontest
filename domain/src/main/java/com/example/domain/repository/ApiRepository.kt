package com.example.domain.repository

import com.example.domain.entities.Action
import com.example.domain.entities.ActionConfig
import io.reactivex.Single

interface ApiRepository {
    fun getActions(): Single<List<ActionConfig>>
}