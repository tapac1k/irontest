package com.example.domain

import com.example.domain.entities.*

class ActionMapper {
    fun map(type: String): Action {
        return when(type) {
            "animation" -> AnimateAction()
            "toast" -> ToastAction()
            "call" -> CallAction()
            "notification" -> NotificationAction()
            else -> error("No such action exception")
        }
    }
}