package com.example.domain.entities

sealed class Action

class AnimateAction: Action()

class ToastAction: Action()

class CallAction: Action()

class NotificationAction: Action()

class None: Action()