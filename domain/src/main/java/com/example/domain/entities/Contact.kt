package com.example.domain.entities

data class Contact(
    val id: Int,
    val name: String,
    val avatar: String?,
    val phone: String
)