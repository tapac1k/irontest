package com.example.data.sources.api

data class ActionResponse(
    val cool_down: Int,
    val enabled: Boolean,
    val priority: Int,
    val type: String,
    val valid_days: List<Int>
)