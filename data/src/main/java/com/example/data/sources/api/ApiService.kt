package com.example.data.sources.api

import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {


    @GET("/androidexam/butto_to_action_config.json")
    fun getActions(): Single<List<ActionResponse>>
}