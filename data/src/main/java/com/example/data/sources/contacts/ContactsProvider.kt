package com.example.data.sources.contacts

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.provider.ContactsContract
import com.example.domain.entities.Contact
import io.reactivex.Single


class ContactsProvider(
    private val context: Context) {

    @SuppressLint("InlinedApi")
    private val FROM_COLUMNS: Array<String> = arrayOf(
        ContactsContract.CommonDataKinds.Phone.NUMBER,
        ContactsContract.CommonDataKinds.Phone._ID,
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.PHOTO_URI
    )

    fun getContacts(): Single<List<Contact>> {
        val contenT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI

        return Single.create { emitter ->
            val contentResolver = context.contentResolver
            var cursor: Cursor? = null
            try {
                cursor = contentResolver.query(
                    contenT_URI,
                    FROM_COLUMNS,
                    "${ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME} IS NOT NULL AND " +
                            "${ContactsContract.CommonDataKinds.Phone.NUMBER} IS NOT NULL",
                    null,
                    """${ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME} ASC"""
                )

                if (cursor == null) {
                    emitter.onSuccess(emptyList())
                    return@create
                }

                val contacts = generateSequence { if (cursor.moveToNext()) cursor else null }
                    .map { mapContactFromCursor(it) }
                    .toList()

                emitter.onSuccess(contacts.distinctBy { it.phone })
            } catch (e: Exception) {
                emitter.onError(e)
            } finally {
                cursor?.close()

            }
        }
    }


    private fun mapContactFromCursor(cursor: Cursor): Contact {
        val id = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID))
        val name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
        val avatar = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))
        val number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

        return Contact(id = id, name = name, phone = number, avatar = avatar)
    }
}
