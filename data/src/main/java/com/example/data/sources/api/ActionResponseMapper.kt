package com.example.data.sources.api

import com.example.domain.entities.ActionConfig

class ActionResponseMapper {
    fun map(action: ActionResponse): ActionConfig {
        return ActionConfig(
            coolDown = action.cool_down,
            enabled = action.enabled,
            priority = action.priority,
            type = action.type,
            validDays = action.valid_days
        )
    }

    fun map(list: List<ActionResponse>): List<ActionConfig> {
        return list.map(::map)
    }
}
