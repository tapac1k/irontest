package com.example.data

import com.example.domain.repository.LocalRepository
import io.reactivex.Completable
import io.reactivex.Single

class LocalRepositoryImpl: LocalRepository {
    private val actionChosen = hashMapOf<String,Long>()

    override fun getLastTimeChosen(action: String): Single<Long> {
        return Single.fromCallable {
            actionChosen.getOrElse(action, { 0L })
        }
    }

    override fun setLastTimeChosen(action: String, time: Long): Completable {
        return Completable.fromAction{
            actionChosen[action] = time
        }
    }
}