package com.example.data

import android.content.Context
import com.example.data.sources.contacts.ContactsProvider
import com.example.domain.entities.Contact
import com.example.domain.repository.PhoneRepository
import io.reactivex.Single
import android.content.Context.CONNECTIVITY_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.net.ConnectivityManager
import java.net.InetAddress


class PhoneRepositoryImpl(
    val context: Context,
    private val contactactsProvider: ContactsProvider
): PhoneRepository {
    override fun getContacts(): Single<List<Contact>> {
        return contactactsProvider.getContacts()
    }

    override fun internetConnectionEnabled(): Single<Boolean> {
        return Single.fromCallable {
            try {
                val ipAddr = InetAddress.getByName("google.com")
                !ipAddr.equals("")
            } catch (e: Exception) {
                false
            }
        }
    }
}