package com.example.data

import com.example.data.sources.api.ActionResponseMapper
import com.example.data.sources.api.ApiService
import com.example.domain.entities.ActionConfig
import com.example.domain.repository.ApiRepository
import io.reactivex.Single

class ApiRepositoryImpl(
    private val apiService: ApiService,
    private val actionMapper: ActionResponseMapper
): ApiRepository {

    override fun getActions(): Single<List<ActionConfig>> {
        return apiService.getActions().map { actionMapper.map(it) }
    }
}